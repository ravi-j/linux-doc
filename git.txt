
					GIT and GITHUB

					What is Git :
					It's a version control system for tracking changes in computer files:
					We can use git for static HTML websites node-js apps python java c-sharp anything to stores
					Many developers can work on a single project without having on the same network.
					Git coordinates work between multiple developers tracks every single version and 
					Every single change that’s made on the system or on the project.
					And any changes done in persons system and upload to or push remote repository to something like GitHub or bitbucket 
					
					--------------------****--------------------

					Concepts of Git:  
					i  . keeps track of code history 
					II. Takes “snapshots” of your files 
					III. you decide when to take a snapshot by making a “commit”
					IV. you can visit any snapshot at any time 
					V.  You can stage files before committing 

					--------------------****--------------------

					Installing Git
					Linux (Debian):$ sudo apt-get install git
					linux (Fedora):$ sudo yum install git

					Launching Git Bash: For windows user
					Create a Project 
					Create Files
					Initialize Git Repository
					Add name and email
					Stage File
					Git Commit
					Git Ignore
					Branches
					Merge
					Remote Repository(GitHub)
					New Repository(GitHub)
					Adding Read Me
					Clone Repository(GitHub)
					Git Pull
					--------------------****--------------------
					Command line instructions
					You can also upload existing files from your computer using the instructions below.
					
					Git global setup
					git config --global user.name "ravikumar"
					git config --global user.email "ravij@unifytech.com"

					Create a new repository
					git clone git@gitlab.com:kommi1/wellzio_sample.git
					cd wellzio_sample
					touch README.md
					git add README.md
					git commit -m "add README"
					git push -u origin master

					Push an existing folder
					cd existing_folder
					git init
					git remote add origin git@gitlab.com:kommi1/wellzio_sample.git
					git add .
					git commit -m "Initial commit"
					git push -u origin master

					Push an existing Git repository
					cd existing_repo
					git remote rename origin old-origin
					git remote add origin git@gitlab.com:kommi1/wellzio_sample.git
					git push -u origin --all
					git push -u origin --tags

					--------------------****--------------------
