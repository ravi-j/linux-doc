var myvideo = document.getElementById('ad');

function playPause(){
    if(myvideo.paused){
        myvideo.play();
    }else{
        myvideo.pause();
    }
}

function largescreen(){
    myvideo.width=560;

}

function smallscreen(){
    myvideo.width=320;

}

function normal(){
    myvideo.width=420;
}